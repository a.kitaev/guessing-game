using System;
using System.Collections;
using System.Collections.Generic;

namespace Guessing_game {
    public class Game {
        private const int Range = 50;
        private readonly List<String> _phrases = new List<String>();
        private readonly ArrayList _history = new ArrayList();

        private string _name;
        private DateTime _startTime;
        private int _errorsCount;
        private int _number;

        public Game() {
            _phrases.Add("My granny would handle faster, ");
            _phrases.Add("Do I sit here until the evening? Ehh, ");
            _phrases.Add("I'm sorry, I'd better go, ");
        }

        public void Start() {
            LetsGameBegin();
            InitValues();
            Guessing();
            Console.WriteLine("Bye!");
        }

        private void LetsGameBegin() {
            Console.Write("Enter your name: ");
            _name = Console.ReadLine();
            Console.WriteLine("Hi, " + _name +
                              "! I make a number from 0 to 50, and you try to guess it? The game starts");
            Console.WriteLine("<< Enter 'q' to leave the game >>");
        }

        private void InitValues() {
            _startTime = DateTime.Now;
            var random = new Random();
            _number = random.Next() % Range;
            _history.Clear();
        }

        private void Guessing() {
            while (true) {
                var line = Console.ReadLine();

                if (line == "q") {
                    return;
                }

                int userNumber;
                try {
                    userNumber = int.Parse(line);
                } catch (FormatException e) {
                    Console.WriteLine("You have to enter numbers, ok?");
                    continue;
                }

                if (userNumber < 0 || userNumber > Range) {
                    Console.WriteLine("Guessing limit 0.." + Range);
                    continue;
                }

                SaveInHistory(userNumber);

                if (userNumber == _number) {
                    break;
                }

                ShowMessage(userNumber);
            }

            ShowStats();
        }

        private void SaveInHistory(int value) {
            var str = " (my num ";
            if (value < _number) {
                str += "^)";
            } else if (value > _number) {
                str += "v)";
            } else {
                str += "=)";
            }

            _history.Add("input: " + value + str);
        }

        private void ShowStats() {
            var interval = DateTime.Now - _startTime;
            Console.WriteLine("Finally, you guessed it, in less than a year!  Or passed ... Let's see");
            Console.WriteLine("You spent: " + interval);
            Console.WriteLine("You made " + (_errorsCount - 1) + " mistakes");
            Console.WriteLine("History:");
            foreach (var o in _history) {
                Console.WriteLine(o);
            }
        }

        private void ShowMessage(int userNumber) {
            _errorsCount++;
            Console.WriteLine(userNumber > _number
                ? "I made a number less than the entered"
                : "I made a number greater than the entered");

            if (_errorsCount % 4 == 0) {
                var random = new Random();
                Console.WriteLine(_phrases[random.Next() % _phrases.Count] + _name);
            }
        }
    }
}